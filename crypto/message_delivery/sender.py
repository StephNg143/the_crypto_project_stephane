from abc import abstractmethod


# This is an abstract class for senders.
# Every sender should inherit from this class.
class Sender:

    # A sender takes a message a send it in its own way,
    # using its own channels.
    @abstractmethod
    def send(self, message: str) -> None:
        pass


# This mock sender is useful for testing only.
class MockSender(Sender):

    # The mock sender only prints the message.
    def send(self, message: str) -> None:
        print(f"Sent using mock sender: {message}")

    
        
        