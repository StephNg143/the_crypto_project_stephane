from typing import Dict
from pydantic import BaseModel, PositiveFloat, root_validator

# This class represents a snapshot of the
# bid and ask limits in an order book
class OrderBook(BaseModel):

    # Four elements are required: the best bid, the best ask and
    # the associated volumes. They are all positive floats.
    bid: PositiveFloat
    ask: PositiveFloat
    bid_volume: PositiveFloat
    ask_volume: PositiveFloat

    # Mid calculated from current bid and ask
    @property
    def mid(self) -> PositiveFloat:
        return (self.bid + self.ask) / 2

    # Bid/ask only accepted if bid < ask
    @root_validator
    def check_ask_gt_bid(cls, values: Dict[str, float]) -> Dict[str, float]:
        if values["bid"] >= values["ask"]:
            raise ValueError("Ask price should be greater than bid price")

        return values
