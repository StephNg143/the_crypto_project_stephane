from abc import abstractmethod

from message_delivery.sender import Sender


# This is an abstract class for events.
# Every event should inherit from this class.
class Event:

    # Events are connected to a list of senders.
    def __init__(self, senders: Sender) -> None:
        self.senders = senders

    # Every event is related to a condition that tells if the
    # event happened or not, depending on market events.
    @property
    @abstractmethod
    def condition(self) -> bool:
        pass

    # If an event happens, it is associated with a message that
    # describes what happened in the market.
    @property
    @abstractmethod
    def message(self) -> str:
        pass

    # Checking an event means verifying either it happened or not.
    # If so, the associated message is sent through every sender
    # that is connected to it.
    def check(self) -> None:
        if self.condition:
            for sender in self.senders:
                sender.send(self.message)


# This mock event is useful for testing only.
class MockEvent(Event):

    # The mock event is always realized.
    @property
    def condition(self) -> bool:
        return True

    # The mock event is associated with a mock message for testing.
    @property
    def message(self) -> str:
        return "A mock message to describe a mock event..."