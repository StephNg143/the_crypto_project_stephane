from time import sleep

from market_events.event import Event

# This class defines the app engine.
class App:

    # The app engine is connected to a list of events.
    def __init__(self, events: Event, time_interval: int) -> None:
        self.events = events
        self.time_interval = time_interval

    # For now, when starting the application, each event
    # is checked only once.
    def start(self) -> None:
        while True:
            for event in self.events:
                event.check()

            sleep(self.time_interval)